import json
import unittest
from pprint import pprint
from json_lhs import to_json
from raw_lhs import raw_lhs


class JsonLhsTestCase(unittest.TestCase):
    def test_01(self):
        rlhs1 = raw_lhs("_m42_en_Stock_Recommendation_42_7")[0]
        actual = to_json(rlhs1)
        expected = json.dumps(self.__expected_tc_01(), sort_keys=True)

        self.assertEqual(actual, expected)

    def __expected_tc_01(self):
        return {
            "Obj_0": {
                "Analyst_Organization_LK.class": [
                    "FinancialServices"
                ],
                "BuyRec_Organization_LK.class": [
                    "FinancialServices"
                ],
                "Down_Organization_LK.class": [
                    "FinancialServices"
                ],
                "Evaluate_Organization_LK.class": [
                    "FinancialServices"
                ],
                "Rating_Organization_LK.class": [
                    "FinancialServices"
                ],
                "SellRec_Organization_LK.class": [
                    "FinancialServices"
                ],
                "Up_Organization_LK.class": [
                    "FinancialServices"
                ],
                "_-42_LK.class": [
                    "FinancialServices"
                ],
                "_-42_Organization_LK.class": [
                    "FinancialServices"
                ]
            },
            "Obj_1": {
                "Analyst_DICT": [
                    ""
                ],
                "BuyRec_DICT": [
                    ""
                ],
                "Down_DICT": [
                    ""
                ],
                "Evaluate_DICT": [
                    ""
                ],
                "Rally_DICT": [
                    ""
                ],
                "Rating_DICT": [
                    ""
                ],
                "SellRec_DICT": [
                    ""
                ],
                "Slump_DICT": [
                    ""
                ],
                "Up_DICT": [
                    ""
                ],
                "_-42_DICT.type": [
                    "_-42_en_pricetarget"
                ]
            },
            "Obj_2": {
                "Analyst_Organization_LK.class": [
                    "BasicMaterials",
                    "ConsumerGoods",
                    "ConsumerServices",
                    "Education",
                    "Energy",
                    "Government",
                    "HealthCare",
                    "Industrials",
                    "Media",
                    "NGOIGO",
                    "ProfessionalServices",
                    "Retail",
                    "Technology",
                    "Telecommunications",
                    "UndefinedOrganisations",
                    "Utilities"
                ],
                "BuyRec_Organization_LK.class": [
                    "BasicMaterials",
                    "ConsumerGoods",
                    "ConsumerServices",
                    "Education",
                    "Energy",
                    "Government",
                    "HealthCare",
                    "Industrials",
                    "Media",
                    "NGOIGO",
                    "ProfessionalServices",
                    "Retail",
                    "Technology",
                    "Telecommunications",
                    "UndefinedOrganisations",
                    "Utilities"
                ],
                "Down_Organization_LK.class": [
                    "BasicMaterials",
                    "ConsumerGoods",
                    "ConsumerServices",
                    "Education",
                    "Energy",
                    "Government",
                    "HealthCare",
                    "Industrials",
                    "Media",
                    "NGOIGO",
                    "ProfessionalServices",
                    "Retail",
                    "Technology",
                    "Telecommunications",
                    "UndefinedOrganisations",
                    "Utilities"
                ],
                "Evaluate_Organization_LK.class": [
                    "BasicMaterials",
                    "ConsumerGoods",
                    "ConsumerServices",
                    "Education",
                    "Energy",
                    "Government",
                    "HealthCare",
                    "Industrials",
                    "Media",
                    "NGOIGO",
                    "ProfessionalServices",
                    "Retail",
                    "Technology",
                    "Telecommunications",
                    "UndefinedOrganisations",
                    "Utilities"
                ],
                "Rating_Organization_LK.class": [
                    "BasicMaterials",
                    "ConsumerGoods",
                    "ConsumerServices",
                    "Education",
                    "Energy",
                    "Government",
                    "HealthCare",
                    "Industrials",
                    "Media",
                    "NGOIGO",
                    "ProfessionalServices",
                    "Retail",
                    "Technology",
                    "Telecommunications",
                    "UndefinedOrganisations",
                    "Utilities"
                ],
                "SellRec_Organization_LK.class": [
                    "BasicMaterials",
                    "ConsumerGoods",
                    "ConsumerServices",
                    "Education",
                    "Energy",
                    "Government",
                    "HealthCare",
                    "Industrials",
                    "Media",
                    "NGOIGO",
                    "ProfessionalServices",
                    "Retail",
                    "Technology",
                    "Telecommunications",
                    "UndefinedOrganisations",
                    "Utilities"
                ],
                "Up_Organization_LK.class": [
                    "BasicMaterials",
                    "ConsumerGoods",
                    "ConsumerServices",
                    "Education",
                    "Energy",
                    "Government",
                    "HealthCare",
                    "Industrials",
                    "Media",
                    "NGOIGO",
                    "ProfessionalServices",
                    "Retail",
                    "Technology",
                    "Telecommunications",
                    "UndefinedOrganisations",
                    "Utilities"
                ],
                "_-42_LK.class": [
                    "BasicMaterials",
                    "ConsumerGoods",
                    "ConsumerServices",
                    "Education",
                    "Energy",
                    "Government",
                    "HealthCare",
                    "Industrials",
                    "Media",
                    "NGOIGO",
                    "ProfessionalServices",
                    "Retail",
                    "Technology",
                    "Telecommunications",
                    "UndefinedOrganisations",
                    "Utilities"
                ],
                "_-42_Organization_LK.class": [
                    "BasicMaterials",
                    "ConsumerGoods",
                    "ConsumerServices",
                    "Education",
                    "Energy",
                    "Government",
                    "HealthCare",
                    "Industrials",
                    "Media",
                    "NGOIGO",
                    "ProfessionalServices",
                    "Retail",
                    "Technology",
                    "Telecommunications",
                    "UndefinedOrganisations",
                    "Utilities"
                ]
            },
            "Obj_3": {
                "Analyst_DICT": [
                    ""
                ],
                "BuyRec_DICT": [
                    ""
                ],
                "Down_DICT": [
                    ""
                ],
                "Evaluate_DICT": [
                    ""
                ],
                "Rally_DICT": [
                    ""
                ],
                "Rating_DICT": [
                    ""
                ],
                "SellRec_DICT": [
                    ""
                ],
                "Slump_DICT": [
                    ""
                ],
                "Up_DICT": [
                    ""
                ],
                "_-42_DICT.type": [
                    "_-42_en_pricetarget"
                ]
            }
        }


if __name__ == '__main__':
    unittest.main()
