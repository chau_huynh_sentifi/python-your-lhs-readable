# README #

### What is this repository for? ###
* If you ever need to troubleshoot a generated GATE/JAPE rule based on our ontology (lexical/name entity) and grammar rules, reading the huge generated JAPE rule maybe a challenge for you.
* This litte snippet takes your raw LHS of the JAPE rule and transforms it into a human readable format of JSON to ease your troubleshooting effort.

### How do I get set up? ###

* No special requirement. Follow the test to run and understand function

### Contribution guidelines ###

* Simply open a PR

### Who do I talk to? ###

* Repo owner or admin
* DE team