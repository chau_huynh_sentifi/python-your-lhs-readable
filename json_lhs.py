import json
import re
from pprint import pprint
from raw_lhs import raw_lhs

def to_json(expr):
    rlhs = __raw_lhs(expr)
    json_lhs = __json_expr(rlhs)
    return json.dumps(json_lhs, sort_keys=True)

def __raw_lhs(expr):
    rlhs = expr.replace("\n", "").split(":Event")[0]
    return __strip(rlhs, "(", ")")

def __strip(str, open_char, close_char):
    ret = str
    if ret:
        ret = ret.strip()
    l = len(ret)
    if l > 2:
        if ret[l - 1] == close_char:
            ret = ret[:l - 1]
        if ret[0] == open_char:
            ret = ret[1:]
    return ret

def __json_expr(expr):
    tail = expr
    p = re.compile(":Obj_\\d+")
    m = p.search(tail)
    ret = {}
    while m:
        key = m.group()[1:]
        (start, end) = m.span()
        value = tail[:start].strip()
        ret[key] = __json_value(value)
        # next round
        if len(tail) > end + 1:
            tail = tail[end + 1:].lstrip()
            m = p.search(tail)
        else:
            m = None
    return ret

def __json_value(str):
    items = __strip(str, "(", ")").split("|")
    ret = {}
    for item in items:
        part = __strip(item, "{", "}").split("==")
        if len(part) == 2:
            value = __strip(part[1], "\"", "\"")
        else:
            value = ""
        key = part[0].strip()
        values = ret.get(key, [])
        values.append(value)
        values.sort()
        ret[key] = values
    return ret


if __name__ == '__main__':
    lhss = raw_lhs("_m42_en_Stock_Recommendation_42_7")
    for item in lhss:
        val = to_json(item)
        pprint(val)
